//
//  QuizTests.swift
//  QuizTests
//
//  Created by Pawel Nozykowski on 03.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import XCTest
import RealmSwift
import RxSwift
@testable import QuizTestable

class QuizTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        let realm = try? Realm()
        try? realm?.write {
            realm?.deleteAll()
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testQuizzes() {
        let service = QuizService(api: API.default)
        let handler = expectation(description: "service")
        
        service.fetchQuizzes().success { (quizes) in
            XCTAssert(quizes.count == 2)
            XCTAssert(quizes.count == quizes.items.count)
            handler.fulfill()
        }.failure { (error) in
            XCTAssert(false, error.localizedDescription)
            handler.fulfill()
        }
        
        waitForExpectations(timeout: 10.0) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    func testQuizzesStatusComparation() {
        guard let quiz = Quiz(JSON: ["id": 0]), let quizOther = Quiz(JSON: ["id": 1]) else {
            XCTAssert(false)
            return
        }
        
        let unknown = QuizzesStatus.unknown
        let error = QuizzesStatus.error(message: "testMessage")
        let loading = QuizzesStatus.loading
        let ready = QuizzesStatus.ready
        let readyToPush = QuizzesStatus.readyToPush(quiz: quiz)
        
        XCTAssertTrue(unknown == unknown)
        XCTAssertFalse(unknown == error)
        XCTAssertFalse(unknown == loading)
        XCTAssertFalse(unknown == ready)
        XCTAssertFalse(unknown == readyToPush)
        
        XCTAssertTrue(error == error)
        XCTAssertFalse(error == QuizzesStatus.error(message: "other message"))
        XCTAssertFalse(error == loading)
        XCTAssertFalse(error == ready)
        XCTAssertFalse(error == readyToPush)
        
        XCTAssertTrue(loading == loading)
        XCTAssertFalse(loading == ready)
        XCTAssertFalse(loading == readyToPush)
        
        XCTAssertTrue(ready == ready)
        XCTAssertFalse(ready == readyToPush)
        
        XCTAssertTrue(readyToPush == readyToPush)
        XCTAssertTrue(readyToPush == QuizzesStatus.readyToPush(quiz: quiz))
        XCTAssertFalse(readyToPush == QuizzesStatus.readyToPush(quiz: quizOther))
    }
    
    func testQuizzesViewModelReload() {
        guard let realm = try? Realm() else {
            XCTAssert(false)
            return
        }
        
        let service = QuizService(api: API.default)
        let modelView = QuizzesViewModel(realm: realm, service: service)
        let disposeBag = DisposeBag()
        let handler = expectation(description: "reload")
        
        var correctStates = [QuizzesStatus.unknown, QuizzesStatus.loading, QuizzesStatus.ready]
        
        modelView.state.asObservable().subscribe { event in
            guard let state = event.element else {
                XCTAssert(false)
                return
            }
            
            guard let correctState = correctStates.first else {
                XCTAssert(false)
                return
            }
            
            XCTAssert(correctState == state)
            correctStates.removeFirst()
        }.addDisposableTo(disposeBag)
        
        modelView.state.asObservable().filter { $0 == QuizzesStatus.ready }.subscribe { _ in
            handler.fulfill()
        }.addDisposableTo(disposeBag)
        
        modelView.reload()
        
        waitForExpectations(timeout: 10.0) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    func testQuizViewModelInvalidData() {
        guard let realm = try? Realm() else {
            XCTAssert(false)
            return
        }
        
        guard let quiz = Quiz(JSON: ["id": 0]) else {
            XCTAssert(false)
            return
        }
        
        quiz.result = QuizResult()
        
        let modelView = QuizViewModel(realm: realm, quiz: quiz)
        XCTAssert(modelView.state.value == QuizStatus.error(message: "Invalid data"))
        XCTAssert(modelView.question.value == nil)
        XCTAssert(modelView.answers.value.isEmpty)
    }
}
