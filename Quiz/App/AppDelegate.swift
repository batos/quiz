//
//  AppDelegate.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 03.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        var config = Realm.Configuration(
            schemaVersion: 1
        )
        
        #if TESTS
            API.default.set(environment: .testing)
            config.inMemoryIdentifier = "MemoryRealm"
        #else
            API.default.set(environment: .production)
        #endif
        
        Realm.Configuration.defaultConfiguration = config
        _ = try? Realm()
        
        let router = Router()
        
        window?.rootViewController = router.start(storyboard: UIStoryboard(name: "Main", bundle: nil))
        window?.makeKeyAndVisible()
        
        return true
    }
}
