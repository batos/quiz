//
//  Utils.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 04.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import UIKit
import Foundation
import SwiftSpinner
import SDWebImage

/// MARK:- Localized string extension
extension String {
    var localized: String {
        return NSLocalizedString(self, value: self, comment: "")
    }
}

/**
 Ability to display errors and messages in UIAlertController form
 */
protocol CanShowAlert {
    func showMessage(_ title: String?, message: String)
}

extension CanShowAlert {
    func showError(_ message: String) {
        showMessage("Error".localized, message: message)
    }
}

extension CanShowAlert where Self: UIViewController {
    func showMessage(_ title: String?, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: { _ in }))
        
        present(alert, animated: true, completion: nil)
    }
}

/**
 Ability to display loading indicator
 */
protocol CanDisplayLoadingIndicator {
    func showLoading(titled title: String)
    func hideLoading()
}

extension CanDisplayLoadingIndicator where Self: UIViewController {
    func showLoading(titled title: String) {
       SwiftSpinner.show(title)
    }
    
    func hideLoading() {
        SwiftSpinner.hide()
    }
}

/**
 Swift3 DispatchOnce
 */
public extension DispatchQueue {
    
    private static var _onceTracker = [String]()
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(token: String, block: (Void) -> Void) {
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}

/**
 String Error
 */
extension String: Error { }

/** 
 Fast accessors
 */
extension Int {
    var d: Double {
        return Double(self)
    }
    
    var f: Float {
        return Float(self)
    }
}

extension Bool {
    var i: Int {
        return self ? 1 : 0
    }
}

extension Double {
    var percentage: Double {
        return self * 100.0
    }
    
    var percentageInt: Int {
        return Int(Foundation.round(percentage))
    }
}

/**
 UIImageView imageURL
 */
extension UIImageView {
    var imageURL: URL? {
        get { return nil }
        set {
            image = nil
            
            guard let url = newValue else {
                return
            }
            
            sd_setIndicatorStyle(.gray)
            sd_setShowActivityIndicatorView(true)
            sd_setImage(with: url) { (_, _, cacheType, _) in
                self.alpha = 0.0
                if cacheType == .none {
                    UIView.animate(withDuration: 0.25) {
                        self.alpha = 1.0
                    }
                }
                else {
                    self.alpha = 1.0
                }
            }
        }
    }
}
 
