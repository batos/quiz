//
//  API.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 03.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

enum APIType {
    case development
    case production
    case testing
}

struct API {
    private(set) static var Endpoints: Endpoints!
    
    static var `default` = {
        return API()
    }()
    
    private var type: APIType {
        didSet { setup(type: type) }
    }
    
    mutating func set(environment: APIType) {
        self.type = environment
    }
    
    private init() {
        type = .development
    }
    
    private func setup(type: APIType) {
        switch type {
        case .production:   API.Endpoints = ProductionEndpoints()
        case .development:  API.Endpoints = DevelopmentEndpoints()
        case .testing:      API.Endpoints = TestingEndpoints()
        }
    }
    
    func requestObject<T: Mappable>(params: [String:Any], endpoint: Endpoint) -> RequestResultObject<T> {
        let result = RequestResultObject<T>()
        
        Alamofire.request(endpoint.urlString, method: endpoint.method, parameters: params, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<T>) in
            switch response.result {
            case .success(let object):
                result.success(object)
            case .failure(let error as NSError):
                result.failure(error)
            default:
                break
            }
        }
        
        return result
    }
}

struct Endpoint {
    let urlString: String
    let method: HTTPMethod
    
    func argumented(args: CVarArg...) -> Endpoint {
        return withVaList(args) {
            let formatted = NSString(format: urlString, arguments: $0) as String
            return Endpoint(urlString: formatted, method: method)
        }
    }
}

protocol Endpoints {
    var quizes: Endpoint { get }
    var quizDetails: Endpoint { get }
}

private struct ProductionEndpoints: Endpoints {
    private static let base = "https://quiz.o2.pl/api"
    
    var quizes      = Endpoint(urlString: "\(base)/v1/quizzes/0/100", method: .get)
    var quizDetails = Endpoint(urlString: "\(base)/v1/quiz/%ld/0", method: .get)
}

private struct DevelopmentEndpoints: Endpoints {
    private static let base = "https://quiz.o2.pl/api"
    
    var quizes      = Endpoint(urlString: "\(base)/v1/quizzes/0/100", method: .get)
    var quizDetails = Endpoint(urlString: "\(base)/v1/quiz/%ld/0", method: .get)
}

private struct TestingEndpoints: Endpoints {
    var quizes      = Endpoint(urlString: Bundle.main.url(forResource: "Quizzes", withExtension: "json")!.absoluteString, method: .get) // swiftlint:disable:this force_unwrapping
    var quizDetails = Endpoint(urlString: Bundle.main.url(forResource: "QuizDetails", withExtension: "json")!.absoluteString, method: .get) // swiftlint:disable:this force_unwrapping
}
