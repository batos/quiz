//
//  QuizCell.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 04.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import UIKit

class QuizCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
}

protocol QuizCellView {
    var title: String? { get }
    var description: String? { get }
    var imageURL: URL? { get }
}

extension QuizCell: HasCellID {
    @nonobjc static var CellID = "quizCell"
}

extension QuizCell: CanConfigureCellWithData {
    typealias T = QuizCellView
    
    func setupWithCellData(_ cellData: QuizCellView) {
        titleLabel.text = cellData.title
        descriptionLabel.text = cellData.description
        photoImageView.imageURL = cellData.imageURL
        
        accessibilityLabel = cellData.title
    }
}
