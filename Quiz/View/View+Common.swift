//
//  Common.swift
//  Become
//
//  Created by Pawel Nozykowski on 21.07.2016.
//  Copyright © 2016 Pawel Nozykowski. All rights reserved.
//

import UIKit

protocol CellData {
    
}

protocol CanConfigureCellWithData {
    associatedtype T
    
    func setupWithCellData(_ cellData: T)
}

protocol HasCellID {
    static var CellID: String { get }
}
