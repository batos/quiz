//
//  AnswerCell.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 07.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import UIKit

class AnswerCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

protocol AnswerCellView {
    var title: String? { get }
}

extension AnswerCell: HasCellID {
    @nonobjc static var CellID = "answerCell"
}

extension AnswerCell: CanConfigureCellWithData {
    typealias T = AnswerCellView
    
    func setupWithCellData(_ cellData: AnswerCellView) {
        titleLabel.text = cellData.title
        
        accessibilityLabel = cellData.title
    }
}
