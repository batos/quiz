//
//  Router.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 04.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import UIKit
import RealmSwift

class Router {
    func start(storyboard: UIStoryboard) -> UIViewController? {
        guard let quizzesViewController = storyboard.instantiateInitialViewController() as? QuizzesViewController else {
            return nil
        }
        
        guard let realm = try? Realm() else {
            return nil
        }
        
        let service = QuizService(api: API.default)
        
        quizzesViewController.model = QuizzesViewModel(realm: realm, service: service)
        return UINavigationController(rootViewController: quizzesViewController)
    }
    
    func show(quiz: Quiz, on viewController: UIViewController) {
        guard let storyboard = viewController.storyboard else {
            return
        }
        
        guard let quizViewController = storyboard.instantiateViewController(withIdentifier: QuizViewController.ViewControllerID) as? QuizViewController else {
            return
        }
        
        guard let realm = try? Realm() else {
            return
        }
        
        quizViewController.model = QuizViewModel(realm: realm, quiz: quiz)
        
        viewController.navigationController?.pushViewController(quizViewController, animated: true)
    }
    
    func showResult(result: Double, on viewController: QuizViewController) {
        let alert = UIAlertController(title: "Brawo", message: String(format: "Odpowiedziałeś na %d%% pytań", result.percentageInt), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Przejdź do listy quizów", style: .cancel, handler: { _ in
            viewController.navigationController?.popViewController(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Rozwiąż jeszcze raz", style: .default, handler: { _ in
            viewController.model.reset()
        }))
        
        viewController.present(alert, animated: true, completion: nil)
    }
}
