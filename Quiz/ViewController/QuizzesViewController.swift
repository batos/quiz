//
//  QuizzesViewController.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 04.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

private struct QuizCellData: QuizCellView {
    var title: String?
    var description: String?
    var imageURL: URL?
    
    init(title: String?, description: String?, imageURL: URL?) {
        self.title = title
        self.description = description
        self.imageURL = imageURL
    }
}

class QuizzesViewController: UIViewController, CanShowAlert, CanDisplayLoadingIndicator {
    @IBOutlet weak var tableView: UITableView!
    
    var model: QuizzesViewModel!
    private let router = Router()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupStateObserver()
        setupQuizzesObserver()
        setupNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.once(token: "quiz.quizzes.load") { [weak self] in
            self?.model.reload()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedRowIndexPath = tableView.indexPathForSelectedRow {
            tableView.reloadRows(at: [selectedRowIndexPath], with: .none)
            tableView.deselectRow(at: selectedRowIndexPath, animated: true)
        }
    }
    
    private func setupStateObserver() {
        model.state.asObservable().subscribe { [weak self] event in
            guard let controller = self, let value = event.element else {
                return
            }
            
            switch value {
            case .loading:
                controller.showLoading(titled: "Wczytywanie...")
            case .ready:
                controller.hideLoading()
                break
            case .error(let message):
                controller.hideLoading()
                controller.showError(message)
                break
            case .readyToPush(let quiz):
                controller.router.show(quiz: quiz, on: controller)
                controller.hideLoading()
            default:
                break
            }
        }.disposed(by: model.disposeBag)
    }
    
    private func description(for quiz: Quiz) -> String {
        let result = model.result(for: quiz)
        
        if result.finished {
            return "Ostatni wynik \(result.correctAnswers)/\(quiz.questions) \((result.correctAnswers.d / quiz.questions.d).percentageInt)%"
        }
        
        return "Quiz rozwiązany w \((result.currentQuestion.d / quiz.questions.d).percentageInt)%"
    }
    
    private func setupQuizzesObserver() {
        model.quizzes.asObservable().bindTo(tableView.rx.items(cellIdentifier: QuizCell.CellID, cellType: QuizCell.self)) { [weak self] _, quiz, cell in
            let data = QuizCellData(title: quiz.title, description: self?.description(for: quiz), imageURL: URL(string: quiz.photoUrl))
            cell.setupWithCellData(data)
        }.addDisposableTo(model.disposeBag)
        
        tableView.rx.modelSelected(Quiz.self).subscribe { [weak self] event in
            guard let controller = self, let quiz = event.element else {
                return
            }

            controller.model.onSelected(quiz: quiz)
        }.addDisposableTo(model.disposeBag)
    }
    
    private func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        tableView.accessibilityLabel = "Quizzes"
    }
    
    private func setupNavigation() {
        navigationItem.title = "Wybierz quiz"
    }
}
