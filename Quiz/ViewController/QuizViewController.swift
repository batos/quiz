//
//  QuizViewController.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 06.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

private struct AnswerCellData: AnswerCellView {
    var title: String?
    
    init(model: Answer) {
        title = model.text
    }
}

class QuizViewController: UIViewController {
    @IBOutlet weak var questionTitleLabel: UILabel!
    @IBOutlet weak var quizProgressView: UIProgressView!
    @IBOutlet weak var tableView: UITableView!
    
    var model: QuizViewModel!
    private let router = Router()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStateObserver()
        setupQuestionObserver()
        setupAnswersObserver()
        setupTableView()
    }
    
    private func setupStateObserver() {
        model.state.asObservable().subscribe { [weak self] event in
            guard let controller = self, let value = event.element else {
                return
            }
            
            switch value {
            case .finished(let result):
                controller.router.showResult(result: result, on: controller)
            case .progress(let question, let of):
                controller.quizProgressView.setProgress(question.f / of.f, animated: false)
            default:
                break
            }
        }.disposed(by: model.disposeBag)
    }
    
    private func setupQuestionObserver() {
        model.question.asObservable().subscribe { [weak self] event in
            guard let controller = self, let question = event.element else {
                return
            }
            
            controller.questionTitleLabel.text = question?.text
            controller.quizProgressView.isHidden = question == nil
        }.addDisposableTo(model.disposeBag)
    }
    
    private func setupAnswersObserver() {
        model.answers.asObservable().bindTo(tableView.rx.items(cellIdentifier: AnswerCell.CellID, cellType: AnswerCell.self)) { _, answer, cell in
            let data = AnswerCellData(model: answer)
            cell.setupWithCellData(data)
        }.addDisposableTo(model.disposeBag)
        
        tableView.rx.modelSelected(Answer.self).subscribe { [weak self] event in
            guard let controller = self, let answer = event.element else {
                return
            }
            
            controller.model.onSelected(answer: answer)
        }.addDisposableTo(model.disposeBag)
    }
    
    private func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.accessibilityLabel = "answers"
    }
}

extension QuizViewController: HasViewControllerID {
     @nonobjc static var ViewControllerID = "quizViewController"
}
