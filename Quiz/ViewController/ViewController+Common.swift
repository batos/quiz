//
//  ViewController+Common.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 06.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import UIKit

protocol HasViewControllerID {
    static var ViewControllerID: String { get }
}
