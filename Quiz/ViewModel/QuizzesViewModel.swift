//
//  QuizzesViewModel.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 04.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

enum QuizzesStatus {
    case unknown
    case loading
    case ready
    case readyToPush(quiz: Quiz)
    case error(message: String)
}

func == (lhs: QuizzesStatus, rhs: QuizzesStatus) -> Bool {
    switch (lhs, rhs) {
    case let (.readyToPush(a), .readyToPush(b)):
        return a.id == b.id
    case let (.error(a), .error(b)):
        return a == b
    case (.unknown, .unknown),
         (.loading, .loading),
         (.ready, .ready):
        return true
    default:
        return false
    }
}

class QuizzesViewModel {
    private let realm: Realm
    private let service: QuizService
    
    let quizzes = Variable<[Quiz]>([])
    let state = Variable<QuizzesStatus>(.unknown)
    let disposeBag = DisposeBag()
    
    init(realm: Realm, service: QuizService) {
        self.realm = realm
        self.service = service
        
        quizzes.value = Array(realm.objects(Quiz.self))
    }
    
    private func load(silence: Bool = false) {
        if !silence {
            state.value = .loading
        }
        
        service.fetchQuizzes()
        .success { [weak self] quizzes in
            self?.quizzes.value = Array(quizzes.items)
            try? self?.quizzes.value.store(update: true)
            self?.state.value = .ready
        }
        .failure { [weak self] error in
            self?.state.value = .error(message: error.localizedDescription)
        }
    }
    
    private func loadDetails(quiz: Quiz) {
        state.value = .loading
        
        service.fetchQuizDetails(quizId: quiz.id)
        .success { [weak self] quizDetails in
            self?.bindDetails(quizDetails: quizDetails, to: quiz)
            self?.state.value = .readyToPush(quiz: quiz)
        }
        .failure { [weak self] error in
            self?.state.value = .error(message: error.localizedDescription)
        }
    }
    
    private func bindDetails(quizDetails: QuizDetails, to quiz: Quiz) {
        realm.beginWrite()
        
        quiz.details?.deleteCascade(realm: realm)
        realm.add(quizDetails, update: true)
        
        quiz.details = quizDetails
        quiz.result = result(for: quiz)
        
        try? realm.commitWrite()
        try? quiz.store(update: true)
    }
    
    func result(for quiz: Quiz) -> QuizResult {
        if let result = realm.object(ofType: QuizResult.self, forPrimaryKey: quiz.id) {
            return result
        }
        
        return QuizResult(value: ["id": quiz.id])
    }
    
    func reload() {
        load()
    }
    
    func onRefresh() {
        load(silence: true)
    }
    
    func onSelected(index: Int) {
        guard index >= 0 && index < quizzes.value.count else {
            state.value = .error(message: "Quiz at index \(index) does not exist")
            return
        }
        
        loadDetails(quiz: quizzes.value[index])
    }
    
    func onSelected(quiz: Quiz) {
        loadDetails(quiz: quiz)
    }
}
