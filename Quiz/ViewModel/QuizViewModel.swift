//
//  QuizViewModel.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 06.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

enum QuizStatus {
    case finished(result: Double)
    case progress(question: Int, of: Int)
    case error(message: String)
    case unknown
}

func == (lhs: QuizStatus, rhs: QuizStatus) -> Bool {
    switch (lhs, rhs) {
    case let (.finished(a), .finished(b)):
        return a == b
    case let (.progress(a, b), .progress(c, d)):
        return a == c && b == d
    case let (.error(a), .error(b)):
        return a == b
    case (.unknown, .unknown):
        return true
    default:
        return false
    }
}

class QuizViewModel {
    private let realm: Realm
    private let quiz: Quiz
    private let result: QuizResult
    
    let state = Variable<QuizStatus>(.unknown)
    let question = Variable<Question?>(nil)
    let answers = Variable<[Answer]>([])
    let disposeBag = DisposeBag()
    
    init(realm: Realm, quiz: Quiz) {
        self.realm = realm
        self.quiz = quiz
        
        guard let result = quiz.result else {
            fatalError("Quiz.Result must be setted")
        }
        
        self.result = result
        
        if result.finished {
            setupFinishedState()
        }
        else {
            setupProgressState()
        }
    }
    
    private func setupFinishedState() {
        state.value = .finished(result: percentageResult)
        question.value = nil
        answers.value = []
    }
    
    private func setupProgressState() {
        state.value = .progress(question: result.currentQuestion, of: quiz.questions)
        
        guard let question = quiz.details?.questions[result.currentQuestion] else {
            state.value = .error(message: "Invalid data")
            return
        }
        
        self.question.value = question
        answers.value = Array(question.answers)
    }
    
    private var percentageResult: Double {
        return result.correctAnswers.d / quiz.questions.d
    }
    
    func onSelected(answer: Answer) {
        realm.beginWrite()
        result.correctAnswers += answer.isCorrent.i
        
        if result.currentQuestion == quiz.questions - 1 {
            result.finished = true
            setupFinishedState()
        }
        else {
            result.currentQuestion += 1
            setupProgressState()
        }
        try? realm.commitWrite()
    }
    
    func reset() {
        realm.beginWrite()
        result.correctAnswers = 0
        result.currentQuestion = 0
        result.finished = false
        try? realm.commitWrite()
        
        setupProgressState()
    }
}
