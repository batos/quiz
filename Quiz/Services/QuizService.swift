//
//  QuizService.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 04.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import Foundation

struct QuizService {
    let api: API
    
    init(api: API) {
        self.api = api
    }
    
    func fetchQuizzes() -> RequestResultObject<Quizes> {
        return api.requestObject(params: [:], endpoint: API.Endpoints.quizes)
    }
    
    func fetchQuizDetails(quizId: Int) -> RequestResultObject<QuizDetails> {
        let argumented = API.Endpoints.quizDetails.argumented(args: quizId)
        return api.requestObject(params: [:], endpoint: argumented)
    }
}
