//
//  FetchData.swift
//  TestProject
//
//  Created by Pawel Nozykowski on 23.09.2016.
//  Copyright © 2016 Pawel Nozykowski. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

protocol Storable {
    func store(update: Bool) throws
}

extension Object: Storable {
    func store(update: Bool = true) throws {
        let realm = try Realm()
        realm.beginWrite()
        realm.add(self, update: update)
        try realm.commitWrite()
    }
}

extension Collection where Self.Iterator.Element: Storable {
    func store(update: Bool = true) throws {
        guard let objects = self as? [Object] else {
            throw "Invalid collection"
        }
        
        let realm = try Realm()
        realm.beginWrite()
        realm.add(objects, update: update)
        try realm.commitWrite()
    }
}

protocol HasAutoId {
    var id: Int { get set }
    var nextId: Int { get }
}

extension HasAutoId where Self: Object {
    var nextId: Int {
        let realm = try? Realm()
        
        let objects = realm?.objects(Self.self).sorted(byKeyPath: "id", ascending: true)
        
        guard let last = objects?.last else {
            return 1
        }
        
        return last.id + 1
    }
}

protocol CascadeDelete {
    func deleteCascade(realm: Realm)
}
