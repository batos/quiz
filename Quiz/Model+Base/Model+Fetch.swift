//
//  Model+Get.swift
//  TestProject
//
//  Created by Pawel Nozykowski on 26.09.2016.
//  Copyright © 2016 Pawel Nozykowski. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class RequestResult<T: Object> {
    var failure: (_ error: NSError) -> Void = { _ in }
    
    @discardableResult
    func failure(failure: @escaping ((_ error: NSError) -> Void)) -> Self {
        self.failure = failure
        return self
    }
}

class RequestResultArray<T: Object>: RequestResult<T> {
    var success: (_ items: [T]) -> Void = { _ in }
    
    @discardableResult
    func success(success: @escaping ((_ items: [T]) -> Void)) -> Self {
        self.success = success
        return self
    }
}

class RequestResultObject<T: Object>: RequestResult<T> {
    var success: (_ object: T) -> Void = { _ in }
    
    @discardableResult
    func success(success: @escaping ((_ object: T) -> Void)) -> Self {
        self.success = success
        return self
    }
}
