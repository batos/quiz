//
//  Category.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 03.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Category: Object, Mappable {
    dynamic var uid = 0
    dynamic var secondaryCid = ""
    dynamic var name = ""
    dynamic var type = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        uid             <- map["uid"]
        secondaryCid    <- map["secondaryCid"]
        name            <- map["name"]
        type            <- map["type"]
    }
    
    override static func primaryKey() -> String? {
        return "uid"
    }
}
