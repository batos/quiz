//
//  Quiz.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 03.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class QuizCategory: Object, Mappable {
    dynamic var id = 0
    dynamic var name = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id      <- map["id"]
        name    <- map["name"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class Quiz: Object, Mappable {
    dynamic var buttonStart = ""
    dynamic var shareTitle = ""
    dynamic var questions = 0
    dynamic var createdAt = Date.distantPast
    dynamic var sponsored = false
    dynamic var id = 0
    dynamic var title = ""
    dynamic var type = ""
    dynamic var content = ""
    dynamic var photoUrl = ""
    dynamic var category: QuizCategory?
    dynamic var details: QuizDetails?
    dynamic var result: QuizResult?
    var categories = List<Category>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        buttonStart     <- map["buttonStart"]
        shareTitle      <- map["shareTitle"]
        questions       <- map["questions"]
        createdAt       <- (map["createdAt"], ISODateTransform())
        sponsored       <- map["sponsored"]
        id              <- map["id"]
        title           <- map["title"]
        type            <- map["type"]
        content         <- map["content"]
        photoUrl        <- map["mainPhoto.url"]
        category        <- map["category"]
        categories      <- (map["categories"], ArrayTransform<Category>())
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

class Quizes: Object, Mappable {
    dynamic var count = 0
    var items = List<Quiz>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        count <- map["count"]
        items <- (map["items"], ArrayTransform<Quiz>())
    }
}
