//
//  QuizResult.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 06.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class QuizResult: Object {
    dynamic var id = 0
    dynamic var finished = false
    dynamic var currentQuestion = 0
    dynamic var correctAnswers = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
