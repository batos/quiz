//
//  QuizDetails.swift
//  Quiz
//
//  Created by Pawel Nozykowski on 05.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Answer: Object, Mappable {
    dynamic var order = 0
    dynamic var text = ""
    dynamic var isCorrent = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        order       <- map["order"]
        text        <- map["text"]
        isCorrent   <- map["isCorrect"]
    }
}

class Question: Object, Mappable, CascadeDelete {
    dynamic var text = ""
    dynamic var answer = ""
    dynamic var type = ""
    dynamic var order = 0
    var answers = List<Answer>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        text    <- map["text"]
        answer  <- map["answer"]
        type    <- map["type"]
        order   <- map["order"]
        answers <- (map["answers"], ArrayTransform<Answer>())
    }
    
    func deleteCascade(realm: Realm) {
        realm.delete(answers)
    }
}

class Rate: Object, Mappable {
    dynamic var from = 0
    dynamic var to = 0
    dynamic var content = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        from        <- map["from"]
        to          <- map["to"]
        content     <- map["content"]
    }
}

class QuizDetails: Object, Mappable, CascadeDelete {
    dynamic var id = 0
    dynamic var createdAt = Date.distantPast
    dynamic var sponsored = false
    dynamic var title = ""
    dynamic var type = ""
    dynamic var content = ""
    var rates = List<Rate>()
    var questions = List<Question>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        createdAt   <- (map["createdAt"], ISODateTransform())
        sponsored   <- map["sponsored"]
        title       <- map["title"]
        type        <- map["type"]
        content     <- map["content"]
        rates       <- (map["rates"], ArrayTransform<Rate>())
        questions   <- (map["questions"], ArrayTransform<Question>())
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func deleteCascade(realm: Realm) {
        realm.delete(rates)
        questions.forEach {
            $0.deleteCascade(realm: realm)
        }
        realm.delete(questions)
    }
}
