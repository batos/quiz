//
//  QuizUITests.swift
//  QuizUITests
//
//  Created by Pawel Nozykowski on 03.04.2017.
//  Copyright © 2017 Pawel Nozykowski. All rights reserved.
//

import XCTest

class QuizUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSolveSimpleQuiz() {
        let app = XCUIApplication()
        app.tables["Quizzes"].staticTexts["Te pytania wydają się banalne. Możesz się jednak oszukać"].tap()
        
        let answersTable = app.tables["answers"]
        answersTable.staticTexts["donkey"].tap()
        answersTable.staticTexts["wallydrag"].tap()
        answersTable.staticTexts["daniel"].tap()
        answersTable.staticTexts["gepard"].tap()
        answersTable.staticTexts["rodent"].tap()
        answersTable.staticTexts["ox"].tap()
        answersTable.staticTexts["szynszyla"].tap()
        
        app.alerts["Brawo"].buttons["Przejdź do listy quizów"].tap()
    }
    
    func testSolveSimpleQuizTwice() {
        let app = XCUIApplication()
        app.tables["Quizzes"].staticTexts["Te pytania wydają się banalne. Możesz się jednak oszukać"].tap()
        
        let answersTable = app.tables["answers"]
        answersTable.staticTexts["donkey"].tap()
        answersTable.staticTexts["wallydrag"].tap()
        answersTable.staticTexts["daniel"].tap()
        answersTable.staticTexts["gepard"].tap()
        answersTable.staticTexts["rodent"].tap()
        answersTable.staticTexts["ox"].tap()
        answersTable.staticTexts["szynszyla"].tap()
        app.alerts["Brawo"].buttons["Przejdź do listy quizów"].tap()
        
        app.tables["Quizzes"].staticTexts["Te pytania wydają się banalne. Możesz się jednak oszukać"].tap()
        app.alerts["Brawo"].buttons["Rozwiąż jeszcze raz"].tap()
        answersTable.staticTexts["donkey"].tap()
        answersTable.staticTexts["wallydrag"].tap()
        answersTable.staticTexts["daniel"].tap()
        answersTable.staticTexts["gepard"].tap()
        answersTable.staticTexts["rodent"].tap()
        answersTable.staticTexts["ox"].tap()
        answersTable.staticTexts["szynszyla"].tap()
        app.alerts["Brawo"].buttons["Przejdź do listy quizów"].tap()
    }
}
